import kidsim
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

#----------------------------------------------------------------MKID-PROPERTIES

band_gap_energy = 1.76 * 1.3806503e-23 * 0.944 #J | ALUMINIUM=1.2K | PTSI=0.944K | HAFNIUM=0.400K | 1.81? (Diener2012_Article_DesignAndTestingOfKineticInduc)
kidsim.quasiparticles_number = 6.626068e-34 * 299792458 / 671e-9 / (2*band_gap_energy) #6489.785474
quasiparticles_per_degree = kidsim.quasiparticles_number / 75 #86.53047299 degree^-1
half_life = 40 #MICROSECONDS | ALUMINIUM=50-100 | PLATINUM SILICIDE=30-40

'''
Aluminium: 14 degrees, 6 keV (1610.00725), 1.2 K (2002-05-Proc-Zmuidzinas)
Platinum Silicide: 75 degrees, 671.0 nm (2002-05-Proc-Zmuidzinas), 0.944K (1610.00725)
Hafnium: 0.400 K (MKIDs_workshop_Dublin_Gregoire)
'''

#----------------------------------------------------------------DELTA-VARIABLES

number_samples = int(1e6)
pulse_rate = 1000 #per second
sampling_frequency = 1e6 #Hz

'''
EXAMPLE pulse_time
MANUAL = [10000,17000,21000,30500,40000,54000,63000,70000,78000,90000]
CONSTANT = np.arange(1000,number_samples-1000,np.int(number_samples/number_pulses))
RANDOM = np.sort(np.random.randint(low=1000,high=number_samples-999,size=number_pulses))

EXAMPLE pulse_phase_shift
MANUAL = np.array([110,80,140,95,190,200,20,90,170,120])
CONSTANT = [75] * number_pulses
RANDOM = np.array((100-50) * np.random.random_sample(size=number_pulses) + 50)
'''

#---------------------------------------------------------------MODULE-VARIABLES

kidsim.recombination_noise_scale = 5
kidsim.electronic_noise_scale = 15
kidsim.electronic_noise_colour = 'white'
kidsim.peak_minimum_phase_shift = 50
kidsim.peak_minimum_sample_distance = int(1.0*half_life)
kidsim.peak_minimum_sample_width = 5
kidsim.rolling_mean_window_width = int(2*half_life)
kidsim.lowpass_cutoff_frequency = 100000
kidsim.minimum_time_histogram_bin_width = 'auto'

#--------------------------------------------------------------MATPLOTLIB-SWITCH

kidsim.matplotlib_plotting = False
kidsim.matplotlib_show_plots = False
matplotlib_plotting = True
matplotlib_show_plots = False

#------------------------------------------------------------------------LOOPING

font = {'size': 16}
matplotlib.rc('font', **font)
plt.rcParams.update({'figure.max_open_warning': 0})

def resolution_loop(number_samples,sampling_frequency,pulse_rate): #VARIABLE
    return(new_resolution)

kidsim.pulse_rate_values = np.arange(10000,100001,5000) #VARIABLE
new_resolution_values = np.zeros_like(kidsim.pulse_rate_values,dtype='float') #VARIABLE
sigma_resolution_values = np.zeros_like(kidsim.pulse_rate_values,dtype='float') #VARIABLE
n_good = np.zeros_like(kidsim.pulse_rate_values,dtype='float') #VARIABLE
n_hist = np.zeros_like(kidsim.pulse_rate_values,dtype='float') #VARIABLE
n_lost = np.zeros_like(kidsim.pulse_rate_values,dtype='float') #VARIABLE

for i in range(0,len(kidsim.pulse_rate_values)): #VARIABLE
    kidsim.pulse_rate = kidsim.pulse_rate_values[i] #VARIABLE
    kidsim.decay_constant = sampling_frequency / (half_life*1e6)
    number_pulses = np.int(np.round(pulse_rate * number_samples * 1e-6))
    kidsim.pulse_time = np.sort(np.random.randint(low=500,high=number_samples-499,size=number_pulses))
    kidsim.pulse_phase_shift = np.array([75] * number_pulses)
    initial_time,initial_phase_shift = kidsim.pulse()
    final_time,final_phase_shift = kidsim.peak(number_samples,sampling_frequency)
    new_resolution_values[i],sigma_resolution_values[i], n_good[i],n_hist[i],n_lost[i] = kidsim.energy_plots(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift)
    n_hist[i] = n_hist[i]/(1.0*n_good[i])
    n_good[i] = n_good[i]/(1.0*pulse_rate)
    n_lost[i] = n_lost[i]/(1.0*pulse_rate)

print('THE pulse_rate_values ARE',kidsim.pulse_rate_values) #VARIABLE
print('THE new_resolution_values ARE',new_resolution_values)
print('THE sigma_resolution_values ARE',sigma_resolution_values)

if matplotlib_plotting:
    plt.figure(figsize=(10,7))
    plt.xlim(min(kidsim.pulse_rate_values)/2,max(kidsim.pulse_rate_values)+min(kidsim.pulse_rate_values)/2) #VARIABLE
    plt.title('Pulse rate against resolution') #VARIABLE
    plt.xlabel('Pulse rate (samples)') #VARIABLE
    plt.ylabel('Resolution')
    plt.plot(kidsim.pulse_rate_values,new_resolution_values,'bo') #VARIABLE
    plt.errorbar(kidsim.pulse_rate_values,new_resolution_values,yerr=sigma_resolution_values,capsize=5) #VARIABLE
    plt.grid()
    plt.savefig('12.png')
if matplotlib_show_plots:
    plt.show()

if matplotlib_plotting:
    plt.figure(figsize=(10,7))
    plt.xlim(min(kidsim.pulse_rate_values)/2,max(kidsim.pulse_rate_values)+min(kidsim.pulse_rate_values)/2) #VARIABLE
    plt.ylim(0,1)
    plt.title('Fraction of true pulses against pulse rate') #VARIABLE
    plt.xlabel('Pulse rate (samples)') #VARIABLE
    plt.ylabel('Fraction')
    plt.plot(kidsim.pulse_rate_values,n_good, color='blue', marker='o', linestyle='-')
    plt.plot(kidsim.pulse_rate_values,1-n_hist, color='green', marker='o', linestyle='--')
    plt.plot(kidsim.pulse_rate_values,n_lost, color='red', marker='o', linestyle='-')
    plt.legend(('Fraction recovered', 'Fraction outside fit', 'Fraction lost'), loc='center left')
    plt.grid()
    plt.savefig('13.png')
if matplotlib_show_plots:
    plt.show()
