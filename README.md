**This repository contains the kinetic inductance detector simulator used in my Master's dissertation.**

**Modelling the photon pile up in a superconducting photon-counting imager**

A dissertation submitted on the April 24 2019 for the degree of Master of Physics, supervised by Professor Kieran O' Brien of Durham University, UK.

This paper investigates the effect of various internal and external variables of MKIDs on their performance. Instead of blindly building physical instruments, we take a step back to the fundamentals and explore the behaviour of MKIDs under various conditions. The current optimal range of photon detection rate is 500 – 1300 photons per second, before being hampered by low SNR and losses over 10%. Increasing an MKID’s quasiparticle half-life brings slight increases in energy resolution, but false positives over 20% set in beyond 80 μs. Electronic noise causes energy resolution to reduce according to expectations, but over 90% of incoming photons can be recovered if rolling-mean filtering is used. However, the same filter caused more harm to the data with losses over 10%, as discovered in low generation-recombination noise regimes. The optimal rolling-mean window width is between half and double the half-life, where an oversized window causes the peak detection and curve fitting to fail, while an undersized window caused the resolution to drop.

**It's still a work in progress, so any improvements are welcome!**
