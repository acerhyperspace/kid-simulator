import numpy as np
import pandas as pd
import scipy.signal as sg
import matplotlib
import matplotlib.pyplot as plt
import acoustics.signal as acs
import acoustics.generator as acg
from lmfit import Model

#--------------------------------------------------------------------------DELTA

font = {'size': 16}
matplotlib.rc('font', **font)
plt.rcParams.update({'figure.max_open_warning': 0})

def pulse():
    return(pulse_time,pulse_phase_shift)

def input(number_samples):
    timestream = np.zeros((number_samples,1))
    for i in range(0,len(pulse_time)):
        timestream[pulse_time[i]] = pulse_phase_shift[i]
    #print('NUMBER OF PULSES IS',len(pulse_time))
    #print('PULSE ARRIVAL TIMES IN SAMPLE TIME ARE',pulse_time)
    #print('CORRESPONDING PULSE PHASE SHIFTS ARE',pulse_phase_shift)
    #print('THE RAW TIMESTREAM IS',timestream)
    #print('THE NUMBER OF QUASIPARTICLES FOR 75 DEGREES AT 671.0 NM IS',quasiparticles_number)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.xlim(0,number_samples)
        plt.title('Timestream of delta pulses')
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(timestream)
        plt.savefig('01.png')
    if matplotlib_show_plots:
        plt.show()
    return(timestream)

#--------------------------------------------------------------------EXPONENTIAL

'''
Reshape is required to convert timestream of (number_samples,1) to (number_samples,) to match dimensions of t
'''

def convolution(number_samples):
    f = input(number_samples).reshape(number_samples,)
    t = np.arange(0,number_samples,1)
    y = np.exp(-(decay_constant) * t)
    conv = sg.fftconvolve(f,y)[0:number_samples]
    print('THE DECAY CONSTANT IS',decay_constant)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.xlim(0,number_samples)
        plt.title('Timestream of delta pulses convoluted with an exponential')
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(conv)
        plt.savefig('02.png')
    if matplotlib_show_plots:
        plt.show()
    return(conv)

'''
Lower phase shift, fewer cooper pairs broken, lower energy resolution
Band gap energy for cooper pair creation in material
'''

#------------------------------------------------------------RECOMBINATION-NOISE

def recombination(number_samples):
    data = convolution(number_samples)
    recombination = np.random.normal(data,recombination_noise_scale*(np.absolute(data))**0.5)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.xlim(0,number_samples)
        plt.title('Timestream of exponential pulses with recombination noise')
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(recombination)
        plt.savefig('03.png')
    if matplotlib_show_plots:
        plt.show()
    return(recombination)

#---------------------------------------------------------------ELECTRONIC-NOISE

def generator(number_samples):
    noise = 0.25 * np.random.random_sample(size=number_samples)
    plt.figure(figsize=(10,7))
    plt.xlim(0,number_samples)
    plt.title('Timestream of noise')
    plt.xlabel('Number of samples')
    plt.ylabel('Phase shift (degrees)')
    plt.plot(noise)
    plt.show()
    return(noise)

def electronic(number_samples):
    noise = electronic_noise_scale * acg.noise(number_samples, color=electronic_noise_colour)
    print('THE ELECTRONIC NOISE FACTOR IS',electronic_noise_scale)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.xlim(0,number_samples)
        plt.title('Timestream of noise')
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(noise)
        plt.savefig('04.png')
    if matplotlib_show_plots:
        plt.show()
    return(noise)

def addition(number_samples,sampling_frequency):
    data = recombination(number_samples)
    total = data + electronic(number_samples)
    total_normalized = total / max(total) * max(data)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.xlim(0,number_samples)
        plt.title('Timestream of delta-exponentials with added noise (Normalized)')
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(total_normalized)
        plt.savefig('05.png')
    if matplotlib_show_plots:
        plt.show()

    x = total_normalized / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='flattop',nperseg=1000)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Power spectral density of timestream with added noise (Normalized)')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power density')
        plt.loglog(f,psd)
        plt.xlim(1,sampling_frequency/2)
        plt.ylim(5e-20,5e-13)
        plt.savefig('06.png')
    if matplotlib_show_plots:
        plt.show()
    return(total_normalized)

'''
Sky background, smaller, more photons
'''

#-------------------------------------------------------------------------FILTER

def psd(number_samples,sampling_frequency):
    data = addition(number_samples)
    x = data / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='hann',nperseg=1000)
    plt.figure(figsize=(10,7))
    plt.title('Overall power spectral density of timestream')
    plt.xlabel('Frequency')
    plt.ylabel('Power density')
    plt.loglog(f,psd)
    plt.xlim(1,sampling_frequency/2)
    plt.ylim(5e-20,5e-13)
    plt.show()

def rolling_mean(number_samples,sampling_frequency):
    data = addition(number_samples,sampling_frequency)
    df = pd.DataFrame(data)
    filter = df.rolling(window=rolling_mean_window_width,win_type=None).mean().shift(-(rolling_mean_window_width-1)).values.reshape(number_samples,)
    timestream = filter[0:number_samples-(rolling_mean_window_width-1)] / max(filter) * max(data)
    #print('THE PROCESSED TIMESTREAM IS',timestream)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Rolling-mean filtered timestream')
        plt.xlim(0,number_samples)
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(timestream)
        plt.savefig('07.png')
    if matplotlib_show_plots:
        plt.show()

    x = timestream / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='hann',nperseg=1000)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Power spectral density of timestream after rolling-mean filtering')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power density')
        plt.loglog(f,psd)
        plt.xlim(1,sampling_frequency/2)
        plt.ylim(5e-20,5e-13)
        plt.savefig('08.png')
    if matplotlib_show_plots:
        plt.show()
    return(timestream)

def lowpass(number_samples,sampling_frequency):
    data = addition(number_samples,sampling_frequency)
    filter = acs.lowpass(signal=data,fs=sampling_frequency,cutoff=lowpass_cutoff_frequency,order=4)
    timestream = filter / max(filter) * max(data)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Lowpass filtered timestream')
        plt.xlim(0,number_samples)
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(timestream)
        plt.savefig('07.png')
    if matplotlib_show_plots:
        plt.show()

    x = timestream / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='hann',nperseg=1000)
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Power spectral density of timestream after lowpass filtering')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power density')
        plt.loglog(f,psd)
        plt.xlim(0,sampling_frequency/2)
        plt.ylim(5e-20,5e-13)
        plt.savefig('08.png')
    if matplotlib_show_plots:
        plt.show()
    return(timestream)

def highpass(number_samples,sampling_frequency):
    data = addition(number_samples,sampling_frequency)
    filter = acs.highpass(signal=data,fs=sampling_frequency,cutoff=100000,order=4)
    timestream = filter / max(filter) * max(data)
    plt.figure(figsize=(10,7))
    plt.title('Highpass filtered timestream')
    plt.xlim(0,number_samples)
    plt.xlabel('Number of samples')
    plt.ylabel('Phase shift (degrees)')
    plt.plot(timestream)
    plt.savefig('07.png')
    plt.show()

    x = timestream / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='hann',nperseg=1000)
    plt.figure(figsize=(10,7))
    plt.title('Power spectral density of timestream after highpass filtering')
    plt.xlabel('Frequency')
    plt.ylabel('Power density')
    plt.loglog(f,psd)
    plt.xlim(0,sampling_frequency/2)
    plt.ylim(5e-20,5e-13)
    plt.savefig('08.png')
    plt.show()
    return(timestream)

def bandpass(number_samples,sampling_frequency):
    data = addition(number_samples,sampling_frequency)
    filter = acs.bandpass(signal=data,fs=sampling_frequency,lowcut=10000,highcut=100000,order=4)
    timestream = filter / max(filter) * max(data)
    plt.figure(figsize=(10,7))
    plt.title('Bandpass filtered timestream')
    plt.xlim(0,number_samples)
    plt.xlabel('Number of samples')
    plt.ylabel('Phase shift (degrees)')
    plt.plot(timestream)
    plt.savefig('07.png')
    plt.show()

    x = timestream / sampling_frequency
    f,psd = sg.welch(x,fs=sampling_frequency,window='hann',nperseg=1000)
    plt.figure(figsize=(10,7))
    plt.title('Power spectral density of timestream after bandpass filtering')
    plt.xlabel('Frequency')
    plt.ylabel('Power density')
    plt.loglog(f,psd)
    plt.xlim(0,sampling_frequency/2)
    plt.ylim(5e-20,5e-13)
    plt.savefig('08.png')
    plt.show()
    return(timestream)

#------------------------------------------------------------------------RECOVER

def correlate(number_samples):
    width = 1000
    data = addition(number_samples)
    t = np.arange(0,width,1)
    y = np.exp(-0.01 * t)
    corr = sg.correlate(data,y)
    corr_normalize = corr / max(corr) - width
    corr_shift = corr_normalize[width:]
    plt.figure(figsize=(10,7))
    plt.title('Normalized correlation of timestream with original exponential')
    plt.xlim(0,number_samples)
    plt.xlabel('Number of samples')
    plt.ylabel('Correlation')
    plt.plot(corr_shift)
    plt.show()

def peak(number_samples,sampling_frequency):
    data = rolling_mean(number_samples,sampling_frequency)
    peak,property = sg.find_peaks(x=data,height=peak_minimum_phase_shift,distance=peak_minimum_sample_distance,width=peak_minimum_sample_width)
    #print(len(peak),'PULSES ARE IDENTIFIED AT',peak,'WITH PHASE SHIFT',data[peak])
    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.title('Timestream with detected peaks marked')
        plt.xlim(0,number_samples)
        plt.xlabel('Number of samples')
        plt.ylabel('Phase shift (degrees)')
        plt.plot(data)
        plt.scatter(peak,data[peak],marker='o')
        plt.savefig('09.png')
    if matplotlib_show_plots:
        plt.show()
    return(peak,data[peak])

# def cwt(number_samples):
#     data = addition(number_samples)
#     wavelet = sg.ricker
#     width = np.arange(1,100)
#     cwt_result = sg.cwt(data,wavelet,width)
#     plt.figure(figsize=(10,7))
#     plt.title('CWT of timestream with Ricker waveform')
#     plt.xlim(0,number_samples)
#     plt.xlabel('Number of samples')
#     plt.ylabel('Width of transformed waveform')
#     plt.imshow(cwt_result,aspect='auto')
#     plt.show()

#------------------------------------------------------------------------COMPARE

def ratio(number_samples,sampling_frequency):
    time_ratio = (final_time - initial_time) / initial_time * 100
    phase_shift_ratio = (final_phase_shift - initial_phase_shift) / initial_phase_shift *100
    overall = [sum(time_ratio) / len(time_ratio),sum(phase_shift_ratio) / len(phase_shift_ratio)]
    print('PERCENTAGE DIFFERENCE OF PULSE TIMES ARE',time_ratio)
    print('PERCENTAGE DIFFERENCE OF PULSE PHASE SHIFTS ARE',phase_shift_ratio)
    print('OVERALL PERCENTAGE DIFFERENCE IN PULSE TIME AND PHASE SHIFT IS',overall)

def difference_time(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift):
    difference_matrix = (final_time[:,np.newaxis] - initial_time)
    #np.savetxt('difference_matrix_time',difference_matrix)
    #print('THE TIME DIFFERENCE MATRIX IS',difference_matrix)
    return(difference_matrix)

# def difference_phase_shift(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift):
#     difference_matrix = (final_phase_shift[:,np.newaxis] - initial_phase_shift)
#     #np.savetxt('difference_matrix_phase_shift',difference_matrix)
#     #print('THE PHASE SHIFT DIFFERENCE MATRIX IS',difference_matrix)
#     return(difference_matrix)

#-----------------------------------------------------------------------FLAGGING

def f_gaussian(x,height,mu,sigma):
    return(height * np.exp(-0.5*((x-mu)/sigma)**2))

def minimum_time(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift):
    initial_index = np.arange(0,len(initial_time),1)
    difference_matrix = difference_time(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift)
    minimum = np.absolute(difference_matrix).min(axis=1)
    true_index = np.argmin(np.absolute(difference_matrix),axis=1)
    lost_index = np.setdiff1d(initial_index,true_index)
    #print('THE MINIMUMS ARE',minimum)
    #print(len(true_index),'PULSES ARE TRUE WITH INITIAL TIMES OF',initial_time[true_index],'AND FINAL TIMES OF',final_time)
    #print('AND CORRESPONDING INITIAL PHASE SHIFTS OF',initial_phase_shift[true_index],'AND FINAL PHASE SHIFTS OF',final_phase_shift)
    #print(len(lost_index),'PULSES ARE LOST WITH INITIAL TIMES OF',initial_time[lost_index])

    x = final_time - initial_time[true_index]
    y = final_phase_shift #- initial_phase_shift[true_index]

    mu = np.mean(final_phase_shift)
    sigma = np.sqrt(np.var(final_phase_shift))
    bin_min = max(0.,int(mu-10*sigma))
    bin_max = int(mu+10*sigma)
    nbin = max(10,10*int(len(true_index)/10))
    hist_pulse_x = np.arange(int(bin_min), int(bin_max))
    hist_pulse_y , hist_pulse_x = np.histogram(y,bins=hist_pulse_x)
    hist_pulse_x = 0.5*(hist_pulse_x[:-1]+hist_pulse_x[1:])

    height = max(hist_pulse_y)
    init_vals = [height,mu,sigma]
    #best_vals, covar = optimize.curve_fit(f_gaussian, hist_pulse_x, hist_pulse_y, p0=init_vals, bounds=[[0.0,0.0,0.1],[len(final_phase_shift),100,50.0]])
    #print('HEIGHT,MU,SIGMA AFTER OPTIMIZING ARE',best_vals)

    gmodel = Model(f_gaussian)
    gmodel.param_names
    params = gmodel.make_params(height=height, mu=mu,sigma=sigma)
    result = gmodel.fit(hist_pulse_y,params,x=hist_pulse_x,calc_covar=True)
    ci = result.conf_interval()
    #print (ci['height'])
    #print(result.fit_report())
    #print(result.conf_interval)
    #print(result)

    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.plot(hist_pulse_x, hist_pulse_y, 'bo')
        plt.plot(hist_pulse_x, result.init_fit, 'k--')
        plt.plot(hist_pulse_x, result.best_fit, 'r-')
        plt.title('Gaussian fit to %s recovered pulses' %len(true_index))
        plt.legend(('Histogram points', 'Histogram outline', 'Gaussian fit'), loc='upper right')
        plt.xlabel('Phase shift (degrees)')
        plt.ylabel('Number of photons')
        plt.savefig('11.png')
    if matplotlib_show_plots:
        plt.show()

    y_height = result.best_values['height']
    var_y_height = ci['height'][3][1] - ci['height'][2][1]
    y_mean = result.best_values['mu']
    var_y_mean = ci['mu'][3][1] - ci['mu'][2][1]
    y_std = abs(result.best_values['sigma'])
    var_y_std = ci['sigma'][3][1] - ci['sigma'][2][1]
    y_fwhm = 2 * np.sqrt(2 * np.log(2)) * y_std
    var_y_fwhm = 2*np.sqrt(2 * np.log(2))*var_y_std
    resolution = y_mean / y_fwhm

    print('THE STANDARD DEVIATION AMPLITUDE DIFFERENCE IS',y_std)
    print('THE FWHM AMPLITUDE DIFFERENCE IS',y_fwhm)
    print('THE FINAL AMPLITUDE MEAN IS',y_mean)
    print('THE ENERGY RESOLUTION IS',resolution)
    sigma_resolution = 2*resolution*(np.sqrt((var_y_fwhm/y_fwhm)**2+(var_y_mean/y_mean)**2))
    print('HEIGHT:',y_height,'+-',var_y_height,'| MU:',y_mean,'+-',var_y_mean,'| SIGMA:',y_std,'+-',var_y_std)
    print('R:',resolution,'+-',sigma_resolution)

    n_good = len(true_index)
    n_hist = int((hist_pulse_x[1]-hist_pulse_x[0])*y_height*np.sqrt(2*3.14159*y_std*y_std))
    n_lost = len(lost_index)

    if matplotlib_plotting:
        plt.figure(figsize=(10,7))
        plt.hist(x, bins=minimum_time_histogram_bin_width, density=False)
        plt.xlim(min(x)-0.5,max(x)+0.5)
        plt.xlabel('Difference in arrival time (samples)')
        plt.ylabel('Number of pulses')
        plt.title('Time difference histogram of %s pulses' %len(final_time))
        plt.savefig('10.png')
    if matplotlib_show_plots:
        plt.show()
    return(x,y,y_mean,y_std,resolution, sigma_resolution, n_good,n_hist,n_lost)

# def minimum_phase_shift(number_samples,sampling_frequency):
#     initial_index = np.arange(0,len(initial_phase_shift),1)
#     difference_matrix = difference_phase_shift(number_samples,sampling_frequency)
#     minimum = np.absolute(difference_matrix).min(axis=1)
#
#     true_index = np.argmin(np.absolute(difference_matrix),axis=1)
#     lost_index = np.setdiff1d(initial_index,true_index)
#     print('THE MINIMUMS ARE',minimum)
#     print(len(true_index),'PULSES ARE TRUE WITH INITIAL PHASE SHIFTS OF',initial_phase_shift[true_index],'AND FINAL PHASE SHIFTS OF',final_phase_shift)
#     print(len(lost_index),'PULSES ARE LOST WITH INITIAL PHASE SHIFTS OF',initial_phase_shift[lost_index])
#
#     x = minimum
#     plt.figure(figsize=(10,7))
#     plt.hist(x, bins='auto', density=False) #bins=np.arange(min(minimum),max(minimum)+1,10)
#     plt.xlim(min(minimum),max(minimum)+1)
#     plt.xlabel('Difference in arrival time (samples)')
#     plt.ylabel('Number of pulses')
#     plt.title('Histogram of %s pulses' %len(final_time))
#     plt.show()
#
#     index = np.argmin(np.absolute(difference_matrix),axis=1)
#     print('THE MINIMUMS ARE',minimum)
#     print(minimum.shape)
#     print(index)
#     print(index.shape)
#     print(len(index),'PULSES ARE TRUE WITH INITIAL PHASE SHIFTS OF',initial_phase_shift[index],'AND FINAL PHASE SHIFTS OF',final_phase_shift)
#     print(len(initial_phase_shift) - len(final_phase_shift),'PULSES ARE LOST')
#     return(minimum)

#--------------------------------------------------------------OUTLIER-FILTERING

def energy_plots(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift):
    x,y,y_mean,y_std,resolution,sigma_resolution, n_good,n_hist,n_lost = minimum_time(number_samples,sampling_frequency,initial_time,initial_phase_shift,final_time,final_phase_shift)
    return(resolution,sigma_resolution, n_good,n_hist,n_lost)
